﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.ML.Data;
using System.Drawing;

namespace KMeans.Pages
{

    public class Data
    {
        [LoadColumn(0)]
        public double Latitudine;

        [LoadColumn(1)]
        public double Longitudine;
    }

    public class ClusterPrediction
    {
        [ColumnName("PredictedLabel")]
        public uint PredictedClusterId;

        [ColumnName("Score")]
        public double[] Distances;
    }
    public class IndexModel : PageModel
    {

        private readonly ILogger<IndexModel> _logger;

        public double[][] rawData { get; set; }

        public int[] clustering;

        public int numClusters;

        public double[][] means;

        public string[] bgColors;




        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            string[] lines = System.IO.File.ReadAllLines(@".\\Data\\Coordinate2.data");

            bgColors = new string[] { "rgb(172, 101, 73)", "rgb(238, 24, 250)", "rgb(224, 142, 128)", "rgb(50, 67, 160)", "rgb(247, 58, 37)" };

            numClusters = 5;

            rawData = new double[lines.Length][];

            int index = 0;
            foreach (var line in lines)
            {
                string[] values = line.Split(';');
                rawData[index] = new double[] { double.Parse(values[0]), double.Parse(values[1]) };
                index++;
            }

            double[][] data = Normalized(rawData);
            bool changed = true; bool success = true;
            clustering = InitClustering(data.Length, numClusters, 0);
            means = Allocate(numClusters, data[0].Length);
            int maxCount = data.Length * 10;
            int ct = 0;
            while (changed == true && success == true && ct < maxCount)
            {
                ++ct;
                success = UpdateMeans(data, clustering, means);
                changed = UpdateClustering(data, clustering, means);
            }
            int numElements = data.Length / numClusters;

            int[][] orderedCentroids = orderCentroidsForNumElements(clustering);

            for(int i = 0; i < numClusters; ++i)
                UpdateClustering2(data, clustering, i, means[i]);

            means = computeCentroids(means, rawData);


            //clustering = Cluster(rawData, means, numClusters);

            var filepath = "Data//clustering.csv";
            using (StreamWriter writer = new StreamWriter(new FileStream(filepath,
            FileMode.Create, FileAccess.Write)))
            {
                writer.WriteLine("sep=;");
                writer.WriteLine("Latitude; Longitude; Clustering");
                for (int i = 0; i < rawData.Length; i++)
                {
                    writer.WriteLine("{0};{1};{2}", rawData[i][0], rawData[i][1], clustering[i]);
                }
            }
        }

        public static int[] Cluster(double[][] rawData, double[][] means, int numClusters)
        {
            double[][] data = Normalized(rawData);
            bool changed = true; bool success = true;
            int[] clustering = InitClustering(data.Length, numClusters, 0);
            means = Allocate(numClusters, data[0].Length);
            int maxCount = data.Length * 10;
            int ct = 0;
            while (changed == true && success == true && ct < maxCount)
            {
                ++ct;
                success = UpdateMeans(data, clustering, means);
                changed = UpdateClustering(data, clustering, means);
            }
            return clustering;
        }

        private static double[][] Normalized(double[][] rawData)
        {
            double[][] result = new double[rawData.Length][];
            for (int i = 0; i < rawData.Length; ++i)
            {
                result[i] = new double[rawData[i].Length];
                Array.Copy(rawData[i], result[i], rawData[i].Length);
            }

            for (int j = 0; j < result[0].Length; ++j)
            {
                double colSum = 0.0f;
                for (int i = 0; i < result.Length; ++i)
                    colSum += result[i][j];
                double mean = colSum / result.Length;
                double sum = 0.0f;
                for (int i = 0; i < result.Length; ++i)
                    sum += (result[i][j] - mean) * (result[i][j] - mean);
                double sd = sum / result.Length;
                for (int i = 0; i < result.Length; ++i)
                    result[i][j] = (result[i][j] - mean) / sd;
            }
            return result;
        }

        private static int[] InitClustering(int numTuples, int numClusters, int seed)
        {
            Random random = new Random(seed);
            int[] clustering = new int[numTuples];
            for (int i = 0; i < numClusters; ++i)
                clustering[i] = i;
            for (int i = numClusters; i < clustering.Length; ++i)
                clustering[i] = random.Next(0, numClusters);
            return clustering;
        }

        static double[][] Allocate(int numClusters,int numColumns)
        {
            double[][] result = new double[numClusters][];
            for (int k = 0; k < numClusters; ++k)
                result[k] = new double[numColumns];
            return result;
        }
        private static bool UpdateMeans(double[][] data, int[] clustering, double[][] means)
        {
            int numClusters = means.Length;
            int[] clusterCounts = new int[numClusters];
            for (int i = 0; i < data.Length; ++i)
            {
                int cluster = clustering[i];
                ++clusterCounts[cluster];
            }

            for (int k = 0; k < numClusters; ++k)
                if (clusterCounts[k] == 0)
                    return false;

            for (int k = 0; k < means.Length; ++k)
                for (int j = 0; j < means[k].Length; ++j)
                    means[k][j] = 0.0f;

            for (int i = 0; i < data.Length; ++i)
            {
                int cluster = clustering[i];
                for (int j = 0; j < data[i].Length; ++j)
                    means[cluster][j] += data[i][j]; // accumulate sum
            }

            for (int k = 0; k < means.Length; ++k)
                for (int j = 0; j < means[k].Length; ++j)
                    means[k][j] /= clusterCounts[k]; // danger of div by 0
            return true;
        }
        static bool UpdateClustering(double[][] data, int[] clustering, double[][] means)
        {
            int numClusters = means.Length;
            bool changed = false;

            int[] newClustering = new int[clustering.Length];
            Array.Copy(clustering, newClustering, clustering.Length);

            double[] distances = new double[numClusters];

            for (int i = 0; i < data.Length; ++i)
            {
                for (int k = 0; k < numClusters; ++k)
                    distances[k] = Distance(data[i], means[k]);

                int newClusterID = MinIndex(distances);
                if (newClusterID != newClustering[i])
                {
                    changed = true;
                    newClustering[i] = newClusterID;
                }
            }

            if (changed == false)
                return false;

            int[] clusterCounts = new int[numClusters];
            for (int i = 0; i < data.Length; ++i)
            {
                int cluster = newClustering[i];
                ++clusterCounts[cluster];
            }

            for (int k = 0; k < numClusters; ++k)
                if (clusterCounts[k] == 0)
                    return false;

            Array.Copy(newClustering, clustering, newClustering.Length);
            return true; // no zero-counts and at least one change
        }

        private static double Distance(double[] tuple, double[] mean)
        {
            double sumSquaredDiffs = 0.0f;
            for (int j = 0; j < tuple.Length; ++j)
                sumSquaredDiffs += Math.Pow((tuple[j] - mean[j]), 2);
            return Math.Sqrt(sumSquaredDiffs);
        }


        private static int MinIndex(double[] distances)
        {
            int indexOfMin = 0;
            double smallDist = distances[0];
            for (int k = 0; k < distances.Length; ++k)
            {
                if (distances[k] < smallDist)
                {
                    smallDist = distances[k];
                    indexOfMin = k;
                }
            }
            return indexOfMin;
        }

        
        private double[][] computeCentroids(double[][] means, double[][] rawData)
        {
            double[][] result = new double[rawData.Length][];
            for (int i = 0; i < rawData.Length; ++i)
            {
                result[i] = new double[rawData[i].Length];
                Array.Copy(rawData[i], result[i], rawData[i].Length);
            }

            double[][] centroids = new double[means.Length][];
            for (int i = 0; i < means.Length; ++i)
            {
                centroids[i] = new double[means[i].Length];
                Array.Copy(means[i], centroids[i], means[i].Length);
            }

            for (int j = 0; j < result[0].Length; ++j)
            {
                double colSum = 0.0f;
                for (int i = 0; i < result.Length; ++i)
                    colSum += result[i][j];
                double mean = colSum / result.Length;
                double sum = 0.0f;
                for (int i = 0; i < result.Length; ++i)
                    sum += (result[i][j] - mean) * (result[i][j] - mean);
                double sd = sum / result.Length;
                for (int i = 0; i < centroids.Length; ++i)
                    centroids[i][j] = (centroids[i][j] * sd) + mean;
            }


            return centroids;
        }

        private int[][] orderCentroidsForNumElements(int[] clustering)
        {
            int[][] result = new int[numClusters][];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = new int[] { clustering.Where(j => j.ToString().Equals(i.ToString())).Count(),i};
            }

            result.OrderBy(i => i[0]);
            
            return result;
        }


        
        static bool UpdateClustering2(double[][] data, int[] clustering, int cluster, double[] centroid)
        {
            bool changed = false;

            int[] newClustering = new int[clustering.Length];
            Array.Copy(clustering, newClustering, clustering.Length);

            double[][] distances = new double[data.Length][];

            for (int i = 0; i < data.Length; ++i)
            {
                distances[i] = new double[] {i,Distance(data[i], centroid) };
            }

            distances = distances.OrderBy(elem => elem[1]).ToArray();
            int numElems = data.Length / 5;
            int j = 0;
            while (newClustering.Where(i => i.ToString().Equals(cluster.ToString())).Count() < numElems && j < data.Length)
            {
                int index = (int) distances[j][0];
                if (newClustering[index] >= cluster || newClustering.Where(i => i.ToString().Equals(newClustering[index].ToString())).Count() > numElems)
                {
                    newClustering[index] = cluster;
                }
                j++;

            }

            Array.Copy(newClustering, clustering, newClustering.Length);
            return true; // no zero-counts and at least one change
        }

    }
}